# food

- traceability, characterization
    - want to measure
        - pollution
            - mercury
            - PCBs
            - pesticides
        - hormones, additives
    - ways to measure
        - XRF?
        - FTIR?
        - Raman?
        - UV/VIS?
        - Bruker, Ocean Optics
    - open designs, producable in a fab lab
    - Filippos


- food production
    - motivation
        - https://fab.city/
    - background
        - plant factory
        - Freight Farms, Growtainer, Open Ag, Aquapioneers, Vigyan Ashram, ...
    - rapid-prototyping
        - http://mtm.cba.mit.edu/
    - potential benefits
        - freshness
        - nutrients
        - taste
        - reduce footprint
        - pedigree
        - perception value
        - economics
    - to start
        - Peruvian Andes aromatics
        - Muna
        - Chincho
        - Huacatay
    - people
        - Julia
            - prototyping from lab
            - staging work in progress
            - measurements, inputs, outputs
        - Jose
            - https://www.tarantarist.com/people/
            - seeds, growing conditions
            - growth rate
            - legal status
            - Oakwood Tavern
                - one wall of restaurant
                - seats
                - end summer 2019
            - Taranta
                - 1 lb/week of mint, rosemary, thyme, cilantro, basil
                - $5-10/lb
                - 120 seats
        - Sherry
            - project alignment
            - ordering
        - Beno
            - plants exploration
            - hardware support
        - Jake
            - instrumentation, actuation, controls
        - Montse
            - Andes
            - processes
    - processes
        - aquaponics
        - hydroponics
        - aeroponics
        - ...
        - tissue culture
    - schedule
        - end 2018 back-of-the-envelope estimates
        - Spring prototype
        - Summer production
        - Oakwood opens end summer 2019

